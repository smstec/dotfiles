#!/bin/bash

set -e

mkdir -p "${HOME}/.logs/"

cwd=$(pwd)

# vim setup
ln -s "$cwd/vim/vimrc" "${HOME}/.vimrc"
ln -s "$cwd/vim" "${HOME}/.vim"
mkdir -p "${HOME}/.vim/autoload"
mkdir -p "${HOME}/.vim/backup"
mkdir -p "${HOME}/.vim/swp"
mkdir -p "${HOME}/.vim/undo"
mkdir -p "${HOME}/.vim/ftplugin"
mkdir -p "${HOME}/.config/"

# Create default AWS configuration file
mkdir -p "${HOME}/.aws"
echo "[default]" > "${HOME}/.aws/config"
echo "region = us-east-1" >> "${HOME}/.aws/config"

mkdir -p "${HOME}/.local/bin"
mkdir -p "${HOME}/.local_apps"

# Download and "install" ShellCheck
mkdir -p "${HOME}/.local_apps/shellcheck"
wget https://github.com/koalaman/shellcheck/releases/download/latest/shellcheck-latest.linux.x86_64.tar.xz \
    -O "${HOME}/.local_apps/shellcheck/shellcheck.tar.xz"
tar xvf "${HOME}/.local_apps/shellcheck/shellcheck.tar.xz"
ln -s "${HOME}/.local_apps/shellcheck/shellcheck-latest/shellcheck" "${HOME}/.local/bin"

curl -LSso "${HOME}/.vim/autoload/pathogen.vim" https://tpo.pe/pathogen.vim
plugins=( "https://github.com/vim-syntastic/syntastic.git"
          "https://github.com/tell-k/vim-autopep8.git"
          "https://github.com/nvie/vim-flake8.git"
          "https://github.com/tpope/vim-fugitive.git"
          "https://github.com/airblade/vim-gitgutter.git"
          "https://github.com/bronson/vim-trailing-whitespace.git"
          "https://github.com/itchyny/lightline.vim.git"
          "https://github.com/preservim/nerdcommenter.git"
          "https://github.com/altercation/vim-colors-solarized.git"
          "https://github.com/flazz/vim-colorschemes.git"
          )
for plugin in "${plugins[@]}"
do
    echo "Installing $plugin"
    folder=$(echo "$plugin" | awk -F/ '{print $NF}' | awk -F. '{print $1}')
    git clone "$plugin" "${HOME}/.vim/bundle/$folder"
done

wget https://raw.githubusercontent.com/levelone/tequila-sunrise.vim/master/colors/tequila-sunrise.vim \
    -O "${HOME}/.vim/bundle/vim-colorschemes/colors/tequila-sunrise.vim"

# symbolic links
symlinks=( 'flake8'
           'sqliterc'
           'dircolors'
           'tmux.conf'
           'theanorc'
           'inputrc'
           'bashrc'
           'bash_profile'
           'pystartup'
           'pylintrc'
           'psqlrc'
           'redshift'
           'pythonpath'
           'tmuxp'
         )
for link in "${symlinks[@]}"
do
    if [ -e "${HOME}/.$link" ]
    then
        echo "Copying .$link to save.$link"
        mv "${HOME}/.$link" "${HOME}/save.$link"
    fi
    echo "Creating symlink ${HOME}/.$link -> dotfiles/misc/$link"
    ln -s "$cwd/misc/$link" "${HOME}/.$link"
done

# One-off fixes
ln -s "${HOME}/.dircolors" "${HOME}/.dir_colors"
ln -s "$cwd/git" "${HOME}/.config/git"

