#!/bin/bash

while ! sudo apt-get update
do
    echo "Trying again in 5s"
    sleep 5
done

#sudo apt-get update

sudo apt-get install -y
    file \
    graphviz \
    rsync \
    ec2-api-tools \
    jq \
    moreutils \
    git \
    mpich \

sudo apt-get install -y --no-install-recommends \
    make \
    build-essential \
    libssl-dev \
    zlib1g-dev \
    libbz2-dev \
    libreadline-dev \
    libsqlite3-dev \
    curl \
    xz-utils \
    libxml2-dev \
    libxmlsec1-dev \
    libffi-dev \
    liblzma-dev

# the sed invocation inserts the lines at the start of the file
# after any initial comment lines
sed -Ei -e '/^([^#]|$)/ {a \
export PYENV_ROOT="$HOME/.pyenv"
a \
export PATH="$PYENV_ROOT/bin:$PATH"
a \
' -e ':a' -e '$!{n;ba};}' ~/.profile
echo 'eval "$(pyenv init --path)"' >>~/.profile

echo 'eval "$(pyenv init -)"' >> ~/.bashrc

cd /home/ubuntu || exit

curl -L https://pyenv.run | bash \
    && pyenv install 3.8.12 \
    && pyenv global 3.8.12 \
    && pyenv rehash

